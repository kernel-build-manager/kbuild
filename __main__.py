import os, json

from libs.core import tools
from libs.core.kernel import Kernel

from libs.environment import Environment

DEFAULT_KBUILD_CONFIG = os.path.join(
    os.path.expanduser("~"),
    os.path.join(".config", "KBuild")
)
DEFAULT_CONFIG_FILE = os.path.join(
    DEFAULT_KBUILD_CONFIG,
    "config.json"
)
DEFAULT_USERFILES = os.path.join(
    os.path.expanduser("~"),
    "KBuild"
)

def show_menu(kernels: list):
    for index in range(0, len(kernels)):
        print(f" {index + 1} | {kernels[index]}")

    _options = {
        "+":" + | Add kernel",
        "0":" 0 | Exit",
    }
    print("\n - -  Options  - -")
    for _key in _options:
        print(_options[_key])

    selected = input(" - Select an option: ")

    valid = False
    while not valid:
        try:
            selected = int(selected)
            if selected <= len(kernels):
                valid = True
                break
        except Exception:
            for _key in _options:
                if selected == _key:
                    valid = True
                    break
            if valid:
                break

        selected = input(" - Enter a valid option: ")
    
    if (selected == 0):
        exit()

    return str(selected)

def verify_string(string, name, spaces = True):
    error = True
    chars = set('\/:*?"<>|')
    if spaces == False:
        chars.add(" ")
    while error:
        if any((c in chars) for c in string) or string == "":
            if spaces == False:
                string = input(' * Enter a valid ' + name +  ' without (\ / : * ? " <> |): ')
            else:
                string = input(' * Enter a valid ' + name +  ' without (\ / : * ? " <> |) and no spaces: ')
        else:
            error = False
    return string
if __name__ == "__main__":
    if (os.path.exists(DEFAULT_KBUILD_CONFIG) and os.path.exists(DEFAULT_CONFIG_FILE)):
        with open(DEFAULT_CONFIG_FILE, 'r') as _config_file:
            _config_data = json.load(_config_file)
    else:
        print(" - KBuild config was not found.")
        _config_data = input(f"\n - Enter a directory for KBuild to work with ({DEFAULT_USERFILES}): ")
        if not _config_data:
            _config_data = DEFAULT_USERFILES
        
        _config_data = {
            "userfiles": _config_data
        }

        os.makedirs(DEFAULT_KBUILD_CONFIG)
        with open(DEFAULT_CONFIG_FILE, 'w') as _config_file:
            json.dump(_config_data, _config_file, indent=4)

    userfiles = _config_data["userfiles"]
    
    env = Environment(userfiles)
    if not (os.path.exists(userfiles)):
        env.setup()

    kernel_list = env.get_kernel_list()

    if not kernel_list:
        print(f" - No kernels found in {env.user_kernels}. Add a new one.")
        selected = "+"
    else:
        selected = show_menu(kernel_list)
        
    if (selected != "+"):
        selected = int(selected)
        kernel_dir = os.path.join(env.user_kernels, kernel_list[selected - 1])
        kernel_config = os.path.join(kernel_dir, "config.json")

        with open(kernel_config, 'r') as config_file:
            kernel_config = json.load(config_file)
    