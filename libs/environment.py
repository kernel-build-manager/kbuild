import os

from libs.core import tools

class Environment():
    def __init__(self, userfiles: str):
        self.user_files = userfiles
        self.user_kernels = os.path.join(self.user_files, "Kernels")
        self.user_toolchains = os.path.join(self.user_files, "Toolchains")

    def setup(self):
        _folders = vars(self)
        for _key in _folders:
            if not (os.path.exists(_folders[_key])):
                os.makedirs(_folders[_key])

    def get_kernel_list(self):
        kernels = []
        for kernel in os.listdir(self.user_kernels):
            kernels.append(kernel)
        return kernels